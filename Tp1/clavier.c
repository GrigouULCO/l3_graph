#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include <math.h>
#include "clavier.h"
#include "globales.h"

void gestionClavier(unsigned char key, int x, int y){	
  switch(key){
	case '+':
	  pointSize++;
	break;

	case'-':
	  if(pointSize>0){pointSize--;};
	break;
  }
  glutPostRedisplay();		
}

void gestionSpeClavier(unsigned char key, int x, int y){	
  //if(p2D.x != UNDEFINED && p2D != UNDEFINED){ 
	switch(key){
	case GLUT_KEY_DOWN:
	  	p2D.y -= 0.1;
		break;

	case GLUT_KEY_UP:
	  	p2D.y += 0.1;
		break;

	case GLUT_KEY_LEFT:
	  	p2D.x -= 0.1;
		break;

	case GLUT_KEY_RIGHT:
	  	p2D.x += 0.1;
		break;
  	}
  //}
  glutPostRedisplay();		
}
