#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include <math.h>
#include "graphique.h"
#include "clavier.h"
#include "souris.h"
#include "globales.h"

void dessiner(void) {
  float mycos,mysin;
  int i;

  glClearColor(0,0,0,0);
  glClear(GL_COLOR_BUFFER_BIT); 
	
  glLineWidth(lineSize);
  glPointSize(pointSize);

  glBegin(GL_POLYGON);
  	glColor3f(0.15,0.15,0.55); 
  	for(i=0;i<360;i++){
		mycos = 0.9*(cos((i*M_PI)/180));
		mysin = 0.9*(sin((i*M_PI)/180));
		glVertex2f(mycos, mysin);
	}
  glEnd() ; 

  glBegin(GL_POLYGON);
  	glColor3f(0.1,0.1,0.1); 
  	glVertex2f(-0.25, -0.25);
	glVertex2f(-0.25, 0.25);
	glVertex2f(0.25, 0.25);
	glVertex2f(0.25, -0.25);
  glEnd() ;

  glBegin(GL_POINTS);
  	glColor3f(1,1,1); 
  	glVertex2f(p2D.x, p2D.y);
  glEnd();

  glFlush() ; 
}
