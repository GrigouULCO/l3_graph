#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include "graphique.h"
#include "clavier.h"
#include "souris.h"
#include "globales.h"

float lineSize = 1.0;
float pointSize = 1.0;

point2D p2D;
point2D translate;

int main (int argc, char *argv[]) {
  /* initialiser glut */
  glutInit (&argc, argv);
  glutInitWindowSize(256,256);
  glutInitWindowPosition(100,100);
  p2D.x = UNDEFINED;
  p2D.y = UNDEFINED;

  /* creer la fenetre */
  glutCreateWindow(argv[0]);

  /* choix de la fonction de rafraichissement */
  glutDisplayFunc(dessiner);
  glutMouseFunc(gestionSouris);
  glutKeyboardFunc(gestionClavier);
  glutSpecialUpFunc(gestionSpeClavier);

  /* demarrer la boucle evenementielle */
  glutMainLoop();
}
