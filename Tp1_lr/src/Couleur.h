#ifndef COULEUR_H

#define COULEUR_H

#include <iostream>

class Couleur{
	
public:
		
	float rouge, vert, bleu;	
		
	Couleur(float r = 0.0, float v = 0.0, float b = 0.0) {
        rouge = r;
        vert = v;
        bleu = b;
    }

};

#endif
