#ifndef INTENSITE_H

#define INTENSITE_H

#include <iostream>

class Intensite{
	
public:
		
	float irouge, ivert, ibleu;	
		
	Intensite(float r = 1.0, float v = 1.0, float b = 1.0) {
        irouge = r;
        ivert = v;
        ibleu = b;
    }

};
#endif
