#ifndef MATERIAU_H

#define MATERIAU_H

#include <iostream>

#include "Couleur.h"

class Materiau{
	
public:

	Couleur Color;	
	float Kd, Ks, S;

	Materiau(Couleur couleur = Couleur(0.8, 0.8, 0.8), float kd = 0.5, float ks = 0.1, float s = 10) {
        Color = couleur;
        Kd = kd;
        Ks = ks;
        S = s;
    }
};

#endif
