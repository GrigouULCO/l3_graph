#ifndef OBJET_H

#define OBJET_H

#include <iostream>

#include "Materiau.h"


class Objet{

public :
	Materiau Mat;
	
	Objet(Materiau mat = Materiau()) {
        Mat = mat;
    }
    
	virtual bool intersection() = 0;

};
#endif 
