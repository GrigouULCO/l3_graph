#ifndef PLAN_H

#define PLAN_H

#include <iostream>

#include "Objet.h"

class Plan : public Objet{
	
public :
	float A,B,C,D;
	
	Plan(Materiau mat = Materiau(), float a = 0.0, float b = 1.0, float c = 0.0, float d = 0.0) {
        Mat = mat;
        A = a;
        B = b;
        C = c;
        D = d;
    }
	
	virtual bool intersection(){
		return false;
	}

};

#endif 
