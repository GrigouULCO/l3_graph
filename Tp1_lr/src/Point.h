#ifndef POINT_H

#define POINT_H

#include <iostream>


class Point{
	
public:
	float X, Y, Z;
	
	Point(float x = 0.0, float y = 0.0, float z = 0.0) {
        X = x;
        Y = y;
        Z = z;
    }

};

#endif
