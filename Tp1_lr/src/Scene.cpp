#include "Scene.h"

#include <fstream>

Scene::Scene(std::string filename){
	std::ifstream fichier(filename, std::ifstream::in);
	
	Materiau materiau;
	
	while(!fichier.eof()){
		
		std::string type;
		fichier >> type;
			
		if(type == "sphere"){
			float x,y,z,r;
			fichier >> x >> y >> z >> r;
		    std::cout << "Sphere: Centre(X:" << x << ", Y:" << y << ", Z:" << z << "), Rayon:" << r << std::endl;
			objets.push_back(new Sphere(materiau, Point(x, y, z), r));
		}
				
		else if(type == "fond"){
			float r, v, b;
			fichier >> r >> v >> b;
			std::cout << "Fond: Couleur(R:" << r << ", V:" << v << ", B:" << b << ")" << std::endl;
			fond = Couleur(r, v, b);
		}	
		
		else if(type == "source"){
			float x, y, z, r, v, b;
			fichier >> x >> y >> z >> r >> v >> b;
			std::cout << "Source: Position(X:" << x <<", Y:" << y << ", Z:" << z << "), Couleur(R:" << r << ", V:" << v << ", B:" << b << ")" << std::endl;
            source = Source(Intensite(r, v, b), Point(x, y, z));
		}
		
		else if(type == "materiau"){
			float aR, aV, aB, kd, ks, s;
			fichier >> aR >> aV >> aB >> kd >> ks >> s;
			std::cout << "Materiau: Couleur(R:" << aR << ", V:" << aV << ", B:" << aB << "), Reflexion Diffuse:" << kd << ", Reflexion Spéculaire:" << ks << ", Brillance:" << s << std::endl;
            materiau = Materiau(Couleur(aR, aV, aB), kd, ks, s);
		}
		
		else if(type == "plan"){
			float a, b, c, d;
            fichier >> a >> b >> c >> d;
            std::cout << "Source: A:" << a <<", B:" << b << ", C:" << c << ", D:" << d << std::endl;
            objets.push_back(new Plan(materiau, a, b, c, d));
		}
		
		else{
            std::string line;
            std::getline(fichier, line);
        }
	}
}

Scene::~Scene() {
    for (Objet* objet : objets)
        delete objet;
    /*
    for (unsigned int i = 0; i < objets.size(); i++)
        delete objet[i];
    */
    objets.clear();
}

