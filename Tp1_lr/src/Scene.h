#ifndef SCENE_H

#define SCENE_H

#include <string>
#include <vector>

#include "Couleur.h"
#include "Sphere.h"
#include "Plan.h"
#include "Source.h"

class Scene{
	
private : 
	std::vector<Objet*> objets;
	Source source; 
	Couleur fond;
		
public:

	Scene(std::string filename);
    ~Scene();
};

#endif 
