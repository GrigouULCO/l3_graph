#ifndef SOURCE_H

#define SOURCE_H


#include "Point.h"
#include "Intensite.h"

class Source{

public :
	
	Point sPos;
	Intensite sInten;
	
	Source(Intensite intensite = Intensite(), Point position = Point()) {
        sInten = intensite;
        sPos = position;
    }

};
#endif 
