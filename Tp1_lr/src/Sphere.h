#ifndef SPHERE_H

#define SPHERE_H

#include <iostream>

#include "Objet.h"
#include "Point.h"


class Sphere : public Objet{

public :
	Point Centre;
	float Rayon;
	
	Sphere(Materiau mat = Materiau(), Point centre = Point(), float rayon = 1.0) {
        Mat = mat;
        Centre = centre;
        Rayon = rayon;
    }
	
	virtual bool intersection(){
		return false;
	}

};
#endif 
