#ifndef VECTEUR_H

#define VECTEUR_H

#include <iostream>

class Vecteur{
	
public:
	float Dx, Dy, Dz;	
	
	Vecteur(float dx = 0.0, float dy = 0.0, float dz = 0.0) {
        Dx = dx;
        Dy = dy;
        Dz = dz;
    }
};
#endif
