#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>

float trans_axeZ;
float angle_rotY;
float angle_rotX;

float kx;
float ky;
float kz;

void gerer_clavier(unsigned char key, int x, int y){
  switch(key){
	case '+':
	  trans_axeZ++;
	break;

	case'-':
	  trans_axeZ--;
	break;

	case'4':
	  angle_rotY -= 10;
	break;

	case'6':
	  angle_rotY += 10;
	break;

	case'8':
	  angle_rotX -= 10;
	break;

	case'2':
	  angle_rotX += 10;
	break;
	}	
	glutPostRedisplay();
}

void gerer_SpeClavier(unsigned char key, int x, int y){
	switch(key){
	case GLUT_KEY_UP:
	  ky *= 2;
	break;
	
	case GLUT_KEY_DOWN:
	  ky /= 2;
	break;
	
	case GLUT_KEY_LEFT:
	  kx /= 2;
	break;
	
	case GLUT_KEY_RIGHT:
	  kx *= 2;
	break;
	
	case GLUT_KEY_PAGE_UP:
	  kz *= 2;
	break;
	
	case GLUT_KEY_PAGE_DOWN:
	  kz /= 2;
	break;
  }
  glutPostRedisplay();		
}
