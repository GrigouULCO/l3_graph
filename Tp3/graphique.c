#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include <stdio.h>
#include "math.h"
#include "touches.h"
#include "defines.h"

float angle_helice,angle_roue,angle_rotation;
int helice_active,roue_active,rotation_active;
int ON,OFF;

/** 
 * Fonction permettant de dessiner un cube centr� sur l'origine 
 * du rep�re de de taille dimxdimxdim.
 * @param dim la taille du c�t� du cube.
 */

static void cube(float dim)
{
  glBegin(GL_QUADS);
  /* face avant rouge */
  glColor3f(1.0, 0.0, 0.0);
  glVertex3f(-dim/2, -dim/2, dim/2);
  glVertex3f( dim/2, -dim/2, dim/2);
  glVertex3f( dim/2,  dim/2, dim/2);
  glVertex3f(-dim/2,  dim/2, dim/2);

  /* face droite verte */
  glColor3f(0.0, 1.0, 0.0);
  glVertex3f(dim/2, -dim/2, dim/2);
  glVertex3f(dim/2, -dim/2, -dim/2);
  glVertex3f(dim/2,  dim/2, -dim/2);
  glVertex3f(dim/2,  dim/2, dim/2);

  /* face gauche jaune */
  glColor3f(1.0, 1.0, 0.0);
  glVertex3f(-dim/2, -dim/2, dim/2);
  glVertex3f(-dim/2,  dim/2, dim/2);
  glVertex3f(-dim/2,  dim/2, -dim/2);
  glVertex3f(-dim/2, -dim/2, -dim/2);

  /* face arriere blanche */
  glColor3f(1.0, 1.0, 1.0);
  glVertex3f(-dim/2, -dim/2, -dim/2);
  glVertex3f(-dim/2,  dim/2, -dim/2);
  glVertex3f( dim/2,  dim/2, -dim/2);
  glVertex3f( dim/2, -dim/2, -dim/2);

  /* face superieure cyan */
  glColor3f(0.0, 1.0, 1.0);
  glVertex3f(-dim/2, dim/2, dim/2);
  glVertex3f( dim/2, dim/2, dim/2);
  glVertex3f( dim/2, dim/2, -dim/2);
  glVertex3f(-dim/2, dim/2, -dim/2);

  /* face inferieure magenta */
  glColor3f(1.0, 0.0, 1.0);
  glVertex3f(-dim/2, -dim/2, dim/2);
  glVertex3f(-dim/2, -dim/2, -dim/2);
  glVertex3f( dim/2, -dim/2, -dim/2);
  glVertex3f( dim/2, -dim/2, dim/2);

  glEnd();
}

void cylindre(float r, float h, int nb){
 glBegin(GL_QUAD_STRIP);
		for(int i=0;i<=360;i+=360/nb){
		float mycos = r*(cos((i*M_PI)/180));
		float mysin = r*(sin((i*M_PI)/180));
		
		glVertex3f(mycos, mysin,0);
		glVertex3f(mycos, mysin,h);
	    }
 glEnd();
}

void fuselage(){
	glColor3f(0.07, 0.07, 0.50);
	glutSolidSphere(2,40,40);
	cylindre(2.0,7,40);
	glTranslatef(0,0,7);
	glutSolidCone(2,5,40,40);
}

void pale(){
	glBegin(GL_POLYGON);
		glVertex3f(0,0,0);
		glVertex3f(0.5,2,0);
		glVertex3f(0,3,0);
		glVertex3f(-0.5,2,0);
    glEnd();
}

void helice(){
	glColor3f(0.60, 0.0, 0.0);
	
	glPushMatrix();
		pale();
		glRotatef(180.0, 1.0, 0.0, 0.0);
		pale();
	glPopMatrix();
	
	glColor3f(0.50, 0.27, 0.15);
	glPushMatrix();
		glTranslatef(0,0,-1);
		glutSolidCone(1,2,10,40);
	glPopMatrix();
}
void roue(float rayonR, float rayonP){
	glColor3f(0.50, 0.27, 0.15);
	glutSolidTorus(rayonP,rayonR,100,100);
	
	glColor3f(0.60, 0.0, 0.0);
	glPushMatrix();
		glRotatef(90, 1.0, 0.0, 0.0);
		glutSolidCone(rayonP/2, rayonR-rayonP, 100, 100);
	glPopMatrix();
	
	glPushMatrix();
		glRotatef(90, -1.0, 0.0, 0.0);
		glutSolidCone(rayonP/2, rayonR-rayonP, 100, 100);
	glPopMatrix();
	
	glPushMatrix();
		glRotatef(90, 0.0, 1.0, 0.0);
		glutSolidCone(rayonP/2, rayonR-rayonP, 100, 100);
	glPopMatrix();
	
	glPushMatrix();
		glRotatef(90, 0.0, -1.0, 0.0);
		glutSolidCone(rayonP/2, rayonR-rayonP, 100, 100);
	glPopMatrix();
}

void aile(){
	glPushMatrix();
		glRotatef(90, 0.0, 0.0, 1.0);
		glRotatef(90, 1.0, 0.0, 0.0);
		glTranslatef(1.5,-2,0);
		glScalef(1.0,3.0,10);
		glutSolidSphere(0.75,100,100);
	glPopMatrix();
}
void avion(){
	fuselage();
	glPushMatrix();
		glTranslatef(0,0,4);
		glRotatef(angle_helice, 0.0, 0.0, 1.0);
		helice();
	glPopMatrix();
	
	glPushMatrix();
		glRotatef(90, 0.0, 1.0, 0.0);
		glTranslatef(0.5,-1,-2.5);
		glRotatef(angle_roue, 0.0, 0.0, 1.0);
		roue(1.25,0.3);
	glPopMatrix();
	
	glPushMatrix();
		glRotatef(90, 0.0, 1.0, 0.0);
		glTranslatef(0.5,-1,2.5);
		glRotatef(angle_roue, 0.0, 0.0, 1.0);
		roue(1.25,0.3);
	glPopMatrix();
	
	aile();
	
}
/** 
 * Fonction permettant de dessiner le rep�re du monde sous 
 * forme d'une croix 3D.
 * @param dim la taille de la ligne repr�sentant un demi-axe.
 */

static void repere(float dim)
{

  glBegin(GL_LINES);
 
  glColor3f(1.0, 1.0, 1.0);
  glVertex3f(-dim, 0.0, 0.0);
  glVertex3f( dim, 0.0, 0.0);
  glVertex3f(0.0,-dim, 0.0);
  glVertex3f(0.0, dim, 0.0);
  glVertex3f(0.0, 0.0, -dim);
  glVertex3f(0.0, 0.0,  dim);

  glEnd();
}



/**
 * Fonction utilis�e pour afficher le monde 3D � visualiser. Cette fonction
 * sera appel�e � chaque fois qu'un "rafraichissement" de la fen�tre
 * d'affichage sera n�cessaire.
 */

void dessiner(void)
{
  /* effacer l'ecran */
  glClearColor(0.0, 0.0, 0.0, 1.0);

  /* raz de la fenetre avec la couleur de fond */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  /* dessin des objets */
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  repere(2.0);

	glTranslatef(0.0,0.0,-40 + trans_axeZ);
	glRotatef(angle_rotY,0,1,0);
	glRotatef(angle_rotX,1,0,0);
	glScalef(kx,ky,kz);

  
  glPushMatrix();
	glTranslatef(
		15*(cos(angle_rotation*M_PI/180)),
		0,
		15*(sin(angle_rotation*M_PI/180)));
	glRotatef(angle_rotation,0,-1,0);
	avion();
  glPopMatrix();

  glFlush();
  glutSwapBuffers();
  return;

}

void retailler(GLsizei largeur, GLsizei hauteur)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  if(largeur > hauteur){
  	gluPerspective(60.f, ((float)largeur/hauteur), 1.f, 200.f);
  	glViewport(((float)largeur-hauteur)/2, 0, hauteur, hauteur);
  }else{
  	gluPerspective(60.f, ((float)largeur/hauteur), 1.f, 200.f);
  	glViewport(0,((float)hauteur-largeur)/2, largeur, largeur);
}
  glutPostRedisplay();
}

void animer(void)
{
	if(helice_active == ON){
		if(angle_helice < 359){
			angle_helice+= 2.0;
		}else{
			angle_helice = 0.0;
		}
	}
	
	if(roue_active == ON){
		if(angle_roue < 359){
			angle_roue+= 2.0;
		}else{
			angle_roue = 0.0;
		}
	}
	
	//if(rotation_active == ON){
		if(angle_rotation < 359){
			angle_rotation+= 1.0;
		}else{
			angle_rotation = 0.0;
		}
	//}
	glutPostRedisplay();
}

