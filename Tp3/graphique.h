#ifndef _GRAPHIQUE_
#define _GRAPHIQUE_ 

#endif
extern float angle_helice;
extern int helice_active;

extern float angle_roue;
extern int roue_active;

extern float rotation_active;
extern int angle_rotation;

extern void dessiner(void);
extern void animer(void);
void retailler(GLsizei largeur, GLsizei hauteur);
