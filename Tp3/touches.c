#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include "graphique.h"
#include "defines.h"

float trans_axeZ;
float angle_rotY;
float angle_rotX;

float kx;
float ky;
float kz;

void gerer_clavier(unsigned char key, int x, int y){
  switch(key){
	case '+':
	  trans_axeZ++;
	break;

	case'-':
	  trans_axeZ--;
	break;

	case'4':
	  angle_rotY -= 10;
	break;

	case'6':
	  angle_rotY += 10;
	break;

	case'8':
	  angle_rotX -= 10;
	break;

	case'2':
	  angle_rotX += 10;
	break;
	
	case'h':
	  if(helice_active == OFF){
		helice_active = ON;
	  }else{
		helice_active = OFF;
	  }
	break;
	
	case'r':
	  if(roue_active == OFF){
		roue_active = ON;
	  }else{
		roue_active = OFF;
	  }
	break;	
	
	case'c':
	  if(rotation_active == OFF){
		rotation_active = ON;
	  }else{
		rotation_active = OFF;
	  }
	break;
	
	glutPostRedisplay();
  }
}


void gerer_SpeClavier(unsigned char key, int x, int y){
	switch(key){
	case GLUT_KEY_UP:
	  ky *= 2;
	break;
	
	case GLUT_KEY_DOWN:
	  ky /= 2;
	break;
	
	case GLUT_KEY_LEFT:
	  kx /= 2;
	break;
	
	case GLUT_KEY_RIGHT:
	  kx *= 2;
	break;
	
	case GLUT_KEY_PAGE_UP:
	  kz *= 2;
	break;
	
	case GLUT_KEY_PAGE_DOWN:
	  kz /= 2;
	break;
  }
  glutPostRedisplay();		
}
